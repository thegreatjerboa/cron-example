FROM alpine
COPY myawesomescript /bin/myawesomescript
COPY root /var/spool/cron/crontabs/root
RUN chmod +x /bin/myawesomescript
CMD crond -l 8 -f
